
class Calculator:
    def __init__(self, x=0, y=0):
        self.x=x
        self.y=y

    def add(self):
        return self.x + self.y

    def subtract(self):
        return self.x - self.y
    
    def pow(self):
        return pow(self.x, self.y)

