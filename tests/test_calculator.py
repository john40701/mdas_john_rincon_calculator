import pytest
from calculator import Calculator

def test_add():
    result=Calculator(4,5).add()
    assert result == 9

def test_substract():
    result=Calculator(4,5).subtract()
    assert result == -1

def test_pow():
    result=Calculator(2,2).pow()
    assert result == 4