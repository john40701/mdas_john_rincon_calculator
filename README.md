# Setup

- Install chocolatey
- `choco install python`
- Install python extension in VSCode
- `pip install pipenv`
- `pipenv install`
- `pipenv shell`
- Select the created environment in VSCode
- `pipenv run pytest`

